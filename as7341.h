/**
 ******************************************************************************
 * @file as7341.h
 * @author  Kuba Roezner
 * @version V1.0
 * @date 20-01-2020
 * @brief   Definitions of commands and functions for sensor access.
 *
 ***************************************************************************************
 * @attention
 * Library can be used in every STM32 application based on HAL (C language)
 ***************************************************************************************
 */

#ifndef __AS7341_H
#define __AS7341_H

/* Includes ------------------------------------------------------------------*/

#include "stm32l4xx_hal.h"
#include "i2c.h"

/** @defgroup AS7341_LIB AS7341
 * @brief Collection of data structures and functions for use AS7641 sensor.
 * @{
 */

/**
 * @defgroup AS7341_MACROS Macros
 * @brief Macros collection
 * @{
 */

/* I2C address */
#define AS7341_I2C_ADDRESS  (uint16_t)(0x39 << 1)	/**< I2C slave address of AS7341 device. */

/* I2C handler */
#define AS7341_I2C_Handler hi2c1

/* Register address */
#define AS7341_REG_ASTATUS 		(uint16_t) 0x60		/**< Address to access ASTATUS register. */

#define AS7341_REG_CH0_DATA_L	(uint16_t) 0x61		/**< Address to access Channel_0_L register. */
#define AS7341_REG_CH0_DATA_H 	(uint16_t) 0x62		/**< Address to access Channel_0_H register. */

#define AS7341_REG_ITIME_L		(uint16_t) 0x63		/**< Address to access ITMIE_L register. */
#define AS7341_REG_ITIME_M 		(uint16_t) 0x64		/**< Address to access ITMIE_M register. */
#define AS7341_REG_ITIME_H 		(uint16_t) 0x65		/**< Address to access ITMIE_H register. */

#define AS7341_REG_CH1_DATA_L	(uint16_t) 0x66		/**< Address to access Channel_1_L register. */
#define AS7341_REG_CH1_DATA_H 	(uint16_t) 0x67		/**< Address to access Channel_1_H register. */

#define AS7341_REG_CH2_DATA_L	(uint16_t) 0x68		/**< Address to access Channel_2_L register. */
#define AS7341_REG_CH2_DATA_H 	(uint16_t) 0x69		/**< Address to access Channel_2_H register. */

#define AS7341_REG_CH3_DATA_L	(uint16_t) 0x6A		/**< Address to access Channel_3_L register. */
#define AS7341_REG_CH3_DATA_H 	(uint16_t) 0x6B		/**< Address to access Channel_3_H register. */

#define AS7341_REG_CH4_DATA_L	(uint16_t) 0x6C		/**< Address to access Channel_4_L register. */
#define AS7341_REG_CH4_DATA_H 	(uint16_t) 0x6D		/**< Address to access Channel_4_H register. */

#define AS7341_REG_CH5_DATA_L	(uint16_t) 0x6E 	/**< Address to access Channel_5_L register. */
#define AS7341_REG_CH5_DATA_H 	(uint16_t) 0x6F		/**< Address to access Channel_5_H register. */

#define AS7341_REG_CONFIG		(uint16_t) 0x70		/**< Address to access CONFIG register. */
#define AS7341_REG_STAT 		(uint16_t) 0x71		/**< Address to access STAT register. */
#define AS7341_REG_EDGE			(uint16_t) 0x72		/**< Address to access EDGE register. */

#define AS7341_REG_GPIO			(uint16_t) 0x73		/**< Address to access GPIO register. */
#define AS7341_REG_LED			(uint16_t) 0x74		/**< Address to access LED register. */

#define AS7341_REG_ENABLE		(uint16_t) 0x80		/**< Address to access Enable and Configuration register. */
#define AS7341_REG_ATIME		(uint16_t) 0x81		/**< Address to access ATIME register. */
#define AS7341_REG_WTIME		(uint16_t) 0x83		/**< Address to access WTIME register. */

#define AS7341_REG_SP_TH_L_LSB	(uint16_t) 0x84		/**< Address to access SP_TH_L_LSB register. */
#define AS7341_REG_SP_TH_L_MSB	(uint16_t) 0x85		/**< Address to access SP_TH_H_MSB register. */
#define AS7341_REG_SP_TH_H_LSB	(uint16_t) 0x86		/**< Address to access SP_TH_L_LSB register. */
#define AS7341_REG_SP_TH_H_MSB	(uint16_t) 0x87		/**< Address to access SP_TH_H_MSB register. */

#define AS7341_REG_AUXID		(uint16_t) 0x90		/**< Address to access AUXID register. */
#define AS7341_REG_REVID		(uint16_t) 0x91		/**< Address to access REVID register. */
#define AS7341_REG_ID			(uint16_t) 0x92		/**< Address to access ID register. */
#define AS7341_REG_STATUS		(uint16_t) 0x93		/**< Address to access STATUS register. */
#define AS7341_REG_ASTATUS_2	(uint16_t) 0x94		/**< Address to access ASTATUS_2 register. */

#define AS7341_REG_G_CH0_DATA_L	(uint16_t) 0x95
#define AS7341_REG_G_CH0_DATA_H (uint16_t) 0x96

#define AS7341_REG_G_CH1_DATA_L	(uint16_t) 0x97
#define AS7341_REG_G_CH1_DATA_H 	(uint16_t) 0x98

#define AS7341_REG_G_CH2_DATA_L	(uint16_t) 0x99
#define AS7341_REG_G_CH2_DATA_H 	(uint16_t) 0x9A

#define AS7341_REG_G_CH3_DATA_L	(uint16_t) 0x9B
#define AS7341_REG_G_CH3_DATA_H 	(uint16_t) 0x9C

#define AS7341_REG_G_CH4_DATA_L	(uint16_t) 0x9D
#define AS7341_REG_G_CH4_DATA_H 	(uint16_t) 0x9E

#define AS7341_REG_G_CH5_DATA_L	(uint16_t) 0x9F
#define AS7341_REG_G_CH5_DATA_H 	(uint16_t) 0xA0

#define AS7341_REG_STATUS_2		(uint16_t) 0xA3		/**< Address to access STATUS_2 register. */
#define AS7341_REG_STATUS_3		(uint16_t) 0xA4		/**< Address to access STATUS_3 register. */
#define AS7341_REG_STATUS_5		(uint16_t) 0xA6		/**< Address to access STATUS_4 register. */
#define AS7341_REG_STATUS_6		(uint16_t) 0xA7		/**< Address to access STATUS_5 register. */

#define AS7341_REG_CFG_0		(uint16_t) 0xA9		/**< Address to access CFG_0 register. */
#define AS7341_REG_CFG_1		(uint16_t) 0xAA		/**< Address to access CFG_1 register. */
#define AS7341_REG_CFG_3		(uint16_t) 0xAC		/**< Address to access CFG_3 register. */
#define AS7341_REG_CFG_6		(uint16_t) 0xAF		/**< Address to access CFG_6 register. */
#define AS7341_REG_CFG_8		(uint16_t) 0xB1		/**< Address to access CFG_9 register. */
#define AS7341_REG_CFG_9		(uint16_t) 0xB2		/**< Address to access CFG_9 register. */
#define AS7341_REG_CFG_10		(uint16_t) 0xB3		/**< Address to access CFG_10 register. */
#define AS7341_REG_CFG_12		(uint16_t) 0xB5		/**< Address to access CFG_12 register. */

#define AS7341_REG_PRES			(uint16_t) 0xBD		/**< Address to access PRES register. */
#define AS7341_REG_GPIO_2		(uint16_t) 0xBE		/**< Address to access GPIO2 register. */
#define AS7341_REG_ASTEP_L		(uint16_t) 0xCA		/**< Address to access ASTEP_L register. */
#define AS7341_REG_ASTEP_H		(uint16_t) 0xCB		/**< Address to access ASTEP_H register. */

#define AS7341_REG_AGC_GAIN_MAX	(uint16_t) 0xCF		/**< Address to access AGC_GAIN_MAX register. */
#define AS7341_AZ_CONFIG		(uint16_t) 0xD6		/**< Address to access AZ_CONFIG register. */
#define AS7341_FD_TIME_1		(uint16_t) 0xD8		/**< Address to access FD_TIME_1 register. */
#define AS7341_FD_TIME_2		(uint16_t) 0xDA		/**< Address to access FD_TIME_2 register. */
#define AS7341_FD_CFG0			(uint16_t) 0xD7		/**< Address to access FIFO_CFG0 register. */
#define AS7341_FD_STATUS		(uint16_t) 0xDB		/**< Address to access FD_STATUS register. */

#define AS7341_INTENAB			(uint16_t) 0xF9		/**< Address to access INTENAB register. */
#define AS7341_CONTROL			(uint16_t) 0xFA		/**< Address to access CONTROL register. */
#define AS7341_FIFO_MAP			(uint16_t) 0xFC		/**< Address to access FIFO_MAP register. */
#define AS7341_FIFO_LVL			(uint16_t) 0xFD		/**< Address to access FIFO_LVL register. */
#define AS7341_FDATA_L			(uint16_t) 0xFE		/**< Address to access FDATA_L register. */
#define AS7341_FDATA_H			(uint16_t) 0xFF		/**< Address to access FDATA_R register. */

#define AS7341_RAM_ADDRESS		(uint16_t) 0x00		/**< RAM address. */

/**
 * @}
 */

/**
 * @defgroup AS7341_ENUMERATION Enums
 * @brief Enums collection
 * @{
 */

/**
 * @enum as7341_ADC_CH_t
 * @brief ADC description to easier configure chain map.
 * @var PIXEL_DISABLE
 * Pixel connected to ground / disabled
 * @var PIXEL_ADC_0
 * Pixel connected to ADC0
 * @var PIXEL_ADC_1
 * Pixel connected to ADC1
 * @var PIXEL_ADC_2
 * Pixel connected to ADC2
 * @var PIXEL_ADC_3
 * Pixel connected to ADC3
 * @var PIXEL_ADC_4
 * Pixel connected to ADC4
 * @var PIXEL_ADC_5
 * Pixel connected to ADC5
 */
typedef enum as7341_ADC_CH_t
{
	PIXEL_DISABLE,
	PIXEL_ADC_0,
	PIXEL_ADC_1,
	PIXEL_ADC_2,
	PIXEL_ADC_3,
	PIXEL_ADC_4,
	PIXEL_ADC_5
}as7341_ADC_CH_t;

/**
 * @}
 */

/**
 * @defgroup AS7341_UNIONS Unions
 * @brief Unions collection
 * @{
 */

/**
 * @}
 */

/**
 * @defgroup AS7341_VARIABLES Variables
 * @brief Variables collection
 * @{
 */

extern as7341_ADC_CH_t as7341_current_chain_map[40];
extern uint16_t as7341_Spectral_Result[6];

/**
 * @}
 */

/**
 * @}
 */
uint8_t AS7341_RAM_Frame_Pack(as7341_ADC_CH_t *chain_map_to_RAM);
HAL_StatusTypeDef AS7341_Power_Up(void);
HAL_StatusTypeDef AS7341_Integration_Time_Set(uint8_t atime, uint16_t astep, uint8_t wtime);
HAL_StatusTypeDef AS7341_Configure_SMUX(void);
HAL_StatusTypeDef AS7341_Start_Measurements(void);
HAL_StatusTypeDef AS7341_Read_Measurements(void);

#endif /* __AS7341_H */
