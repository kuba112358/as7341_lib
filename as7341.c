/**
 ******************************************************************************
 * @file as7341.h
 * @author  Kuba Roezner
 * @version V1.0
 * @date 22-01-2020
 * @brief AS7341 sensor library.
 *
 ***************************************************************************************
 * @attention
 * Library can be used in every STM32 application based on HAL (C language)
 ***************************************************************************************
 */

#include "as7341.h"

as7341_ADC_CH_t as7341_current_chain_map[40] = { 0 };
static uint8_t as7341_RAM_Frame[20];
uint16_t as7341_Spectral_Result[6] = { 0 };

/**
 * @addtogroup AS7341_LIB
 * @{
 */

/**
 * @defgroup AS7341_FUN   Functions
 * @brief Functions collection
 * @{
 */

/**
 * @brief Description of AS7341_RAM_Frame_Pack.
 * @param as7341_ADC_CH_t *chain_map_to_RAM -> pointer to chain map which will be prepare to RAM frame
 * @retval uint8_t -> status 0 - OK, 1 - Error
 */
uint8_t AS7341_RAM_Frame_Pack(as7341_ADC_CH_t *chain_map_to_RAM)
{
	uint8_t i,j = 0;

	for(i = 0; i < 20; i++)
	{
		if( *(chain_map_to_RAM + j) || *(chain_map_to_RAM + j + 1) > 6)
		{
			return 1;
		}

		as7341_RAM_Frame[i] = ( *(chain_map_to_RAM + j + 1) << 4) | *(chain_map_to_RAM + j);
		j += 2;
	}

	return 0;
}

HAL_StatusTypeDef AS7341_Power_Up(void)
{
	uint8_t data = 0x01;

	return HAL_I2C_Mem_Write(&AS7341_I2C_Handler, AS7341_I2C_ADDRESS, AS7341_REG_ENABLE, 1, &data, 1, 100);
	//return AS7341_i2c_write(AS7341_I2C_ADDRESS, const uint8_t *data, uint8_t count);
}

/**
 * @brief Integration time is define as:
 * time_in = (ATIME + 1) x (ASTEP + 1) x 2.78us
 * wtime is time between two other measurement. base time is 2.78 ms
 * @param as7341_ADC_CH_t *chain_map_to_RAM -> pointer to chain map which will be prepare to RAM frame
 * @retval uint8_t -> status 0 - OK, 1 - Error
 */
HAL_StatusTypeDef AS7341_Integration_Time_Set(uint8_t atime, uint16_t astep, uint8_t wtime)
{
	HAL_StatusTypeDef status;
	uint8_t data[2];
	if( atime == 0 && astep == 0)
		return HAL_ERROR;

	/* Write ATIME */
	status = HAL_I2C_Mem_Write(&AS7341_I2C_Handler, AS7341_I2C_ADDRESS, AS7341_REG_ATIME, 1, &atime, 1, 100);

	/* Write ASTEP sprawdzic jak wysyla*/
	data[0] = astep;
	data[1] = astep >> 8;

	status = HAL_I2C_Mem_Write(&AS7341_I2C_Handler, AS7341_I2C_ADDRESS, AS7341_REG_ASTEP_L, 1, data, 2, 100);

	status = HAL_I2C_Mem_Write(&AS7341_I2C_Handler, AS7341_I2C_ADDRESS, AS7341_REG_WTIME, 1, &wtime, 1, 100);

	/* turn on wait time */

	//read enable register, set WEN bit, write enable register
	status = HAL_I2C_Mem_Read(&AS7341_I2C_Handler, AS7341_I2C_ADDRESS, AS7341_REG_ENABLE, 1, data, 1, 100);

	data[0] |= (1 << 3);

	status = HAL_I2C_Mem_Write(&AS7341_I2C_Handler, AS7341_I2C_ADDRESS, AS7341_REG_ENABLE, 1, data, 1, 100);

	return status;
}



HAL_StatusTypeDef AS7341_Configure_SMUX(void)
{
	HAL_StatusTypeDef status;
	uint8_t data = 0x10;

	/* Write SMUX configuration from RAM to SMUX chain */
	status = HAL_I2C_Mem_Write(&AS7341_I2C_Handler, AS7341_I2C_ADDRESS, AS7341_REG_CFG_6, 1, &data, 1, 100);

	/* Write configuration to RAM */
	status = HAL_I2C_Mem_Write(&AS7341_I2C_Handler, AS7341_I2C_ADDRESS, 0x00, 1, as7341_RAM_Frame, 20, 2000);

	//read enable register, set SMUXEN bit, write enable register
	status = HAL_I2C_Mem_Read(&AS7341_I2C_Handler, AS7341_I2C_ADDRESS, AS7341_REG_ENABLE, 1, &data, 1, 100);

	data |= (1 << 4);

	status = HAL_I2C_Mem_Write(&AS7341_I2C_Handler, AS7341_I2C_ADDRESS, AS7341_REG_ENABLE, 1, &data, 1, 100);

	return status;
}

HAL_StatusTypeDef AS7341_Start_Measurements(void)
{
	HAL_StatusTypeDef status;
	uint8_t data = 0;

	/* wait until SMUXEN command will be finished */
	do
	{
		status = HAL_I2C_Mem_Read(&AS7341_I2C_Handler, AS7341_I2C_ADDRESS, AS7341_REG_ENABLE, 1, &data, 1, 100);
	}while(!(data & (1 << 4)));

	/* set and write SP_EN bit */
	data |= (1 << 1);
	status = HAL_I2C_Mem_Write(&AS7341_I2C_Handler, AS7341_I2C_ADDRESS, AS7341_REG_ENABLE, 1, &data, 1, 100);

	return status;
}

HAL_StatusTypeDef AS7341_Read_Measurements(void)
{
	HAL_StatusTypeDef status;
	uint8_t data = 0, i,j;
	uint8_t color_data[12] = {0};

	/* wait until READY bit will be set */
	do
	{
		status = HAL_I2C_Mem_Read(&AS7341_I2C_Handler, AS7341_I2C_ADDRESS, AS7341_REG_STAT, 1, &data, 1, 100);
	}while(data & (1 << 0));

	status = HAL_I2C_Mem_Read(&AS7341_I2C_Handler, AS7341_I2C_ADDRESS, AS7341_REG_ENABLE, 1, &data, 1, 100);
	/* reset and write SP_EN bit */
	data &= ~(1 << 1);
	status = HAL_I2C_Mem_Write(&AS7341_I2C_Handler, AS7341_I2C_ADDRESS, AS7341_REG_ENABLE, 1, &data, 1, 100);

	/*read measure */
	status = HAL_I2C_Mem_Read(&AS7341_I2C_Handler, AS7341_I2C_ADDRESS, AS7341_REG_G_CH0_DATA_L, 1, color_data, 12, 1200);

	for( i = 0, j = 0; i < 6; i++, j+=2)
	{
		as7341_Spectral_Result[i] = color_data[j + 1] << 8 | color_data[j];
	}

	return status;
}


/**
 * @}
 */

/**
 * @}
 */
